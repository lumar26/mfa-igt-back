package rs.fon.team1.mfaigt.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import rs.fon.team1.mfaigt.model.User;
import rs.fon.team1.mfaigt.dto.AccountCredentialsDto;
import rs.fon.team1.mfaigt.dto.CodeCredentialsDto;
import rs.fon.team1.mfaigt.dto.CredentialsDto;
import rs.fon.team1.mfaigt.dto.UserDto;
import rs.fon.team1.mfaigt.service.AccountService;
import rs.fon.team1.mfaigt.service.AuthService;
import rs.fon.team1.mfaigt.service.CodeValidationService;

@RestController
@RequestMapping("/auth")
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT,
        RequestMethod.DELETE, RequestMethod.PATCH})
public class AuthController {
    private final AuthService authService;
    private final AccountService accountService;
    private final CodeValidationService codeValidationService;

    public AuthController(AuthService authService, AccountService accountService, CodeValidationService codeValidationService) {
        this.authService = authService;
        this.accountService = accountService;
        this.codeValidationService = codeValidationService;
    }

    //    metoda za slanje url i password, prvi faktor
    @PostMapping("/login") //metoda je sad POST, moze i dodaatna putanja
    public ResponseEntity<?> login(@RequestBody CredentialsDto credentials) {
        System.out.println(credentials);
        User user;
        try {
            user = authService.login(credentials);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
        return ResponseEntity.ok(new UserDto(user.getUsername())); // 200 ok
    }

    @PostMapping("/verify") //POST zahtev
    public ResponseEntity<?> verify(@RequestBody AccountCredentialsDto credentials) throws Exception {
        System.out.println(credentials);
        User user = authService.login(new CredentialsDto(credentials.getUsername(), credentials.getPassword()));
        try {
            accountService.checkBalance(credentials);
        } catch (Exception e) {
            int code = (int) Math.floor(Math.random() * 1000000);
            System.err.println("Code: " + code);
            CodeValidationService.addCode(user.getUsername(), code);
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
        UserDto principal = new UserDto(user.getUsername());
//        principal.setToken("token");
        return ResponseEntity.ok(principal); // 200 ok
    }

    @PostMapping("/login-code") //POST zahtev
    public ResponseEntity<?> loginCode(@RequestBody CodeCredentialsDto credentials) throws Exception {
        User user = authService.login(new CredentialsDto(credentials.getUsername(), credentials.getPassword()));
        try {
            codeValidationService.checkVerificationCode(credentials);
        } catch (Exception e){
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
        UserDto principal = new UserDto(user.getUsername());
//        principal.setToken("token");
        return ResponseEntity.ok(principal); // 200 ok
    }
}
