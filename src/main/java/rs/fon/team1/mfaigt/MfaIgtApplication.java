package rs.fon.team1.mfaigt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MfaIgtApplication {

    public static void main(String[] args) {
        SpringApplication.run(MfaIgtApplication.class, args);
    }

}
