package rs.fon.team1.mfaigt.service;

import org.springframework.stereotype.Service;
import rs.fon.team1.mfaigt.model.User;
import rs.fon.team1.mfaigt.dto.CredentialsDto;

import java.util.HashMap;

@Service
public class AuthService {

    private final HashMap<String, User> users = new HashMap<>();

    public AuthService() {
        users.put("luka", new User("luka", "luka"));
        users.put("jovana", new User("jovana", "jovana"));
        users.put("aleksa", new User("aleksa", "aleksa"));
    }

    public User login(CredentialsDto credentials) throws Exception {
        // provera da li korinsik postoji u bazi sa datim kredencijalima
        User user = users.get(credentials.getUsername());
        if (user == null) throw new Exception("User not found");

        if (!user.getPassword().equals(credentials.getPassword())) throw new Exception("Wrong credentials");
        return user;
    }

}
