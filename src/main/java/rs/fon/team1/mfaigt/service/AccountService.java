package rs.fon.team1.mfaigt.service;

import org.springframework.stereotype.Service;
import rs.fon.team1.mfaigt.dto.AccountCredentialsDto;

import java.util.HashMap;

@Service
public class AccountService {
    private final HashMap<String, Double> accounts = new HashMap<>();

    public AccountService() {
        accounts.put("luka", 400000.0);
        accounts.put("jovana", 16000.0);
        accounts.put("aleksa", 3500.0);
        accounts.put("marko", 48000.0);
    }

    public void checkBalance(AccountCredentialsDto credentials) throws Exception {
        if (credentials.getBalance() <= 0) throw new Exception("Balance cannot be less than 0");
        double currentBalance = accounts.get(credentials.getUsername());
        double passedBalance = credentials.getBalance();
        System.out.println("Passed balance: " + passedBalance);
        if (!(credentials.getBalance() == accounts.get(credentials.getUsername())
                || (passedBalance > (currentBalance - calculateHalfRange(currentBalance))
                && passedBalance < (currentBalance + calculateHalfRange(currentBalance)))))
            throw new Exception("Wrong guess for account balance");
    }

    private double calculateHalfRange(double currentBalance) {

//        logika za izracunavanje opsega u kojem mora da se nalazi pogodjena suma
        int length = (int) (Math.log10(currentBalance) + 1);
        if (length == 0 || length == 1) return 0;
        if (length == 2) return 5.0;
        return Math.pow(10, length - 2);
    }
}
