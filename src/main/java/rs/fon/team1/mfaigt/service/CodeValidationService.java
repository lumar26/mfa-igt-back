package rs.fon.team1.mfaigt.service;

import org.springframework.stereotype.Service;
import rs.fon.team1.mfaigt.dto.CodeCredentialsDto;

import java.util.HashMap;

@Service
public class CodeValidationService {

    private static final HashMap<String, Integer> codes = new HashMap<>();

    public static void addCode(String username, Integer code){
        codes.put(username, code);
    }

    public void checkVerificationCode(CodeCredentialsDto credentials) throws Exception {
        if (codes.isEmpty() || credentials.getCode() != codes.get(credentials.getUsername())) throw new Exception("Wrong verification token");
    }
}
