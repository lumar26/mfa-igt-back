package rs.fon.team1.mfaigt.dto;

public class UserDto {
    private final String username;
    private String token;

    public UserDto(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
