package rs.fon.team1.mfaigt.dto;

import java.io.Serializable;

public class CredentialsDto implements Serializable {
    private String username;
    private String password;

    public CredentialsDto() { // prazan zbog serijalizacije koju radi Jackson
    }

    public CredentialsDto(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "Credentials{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
